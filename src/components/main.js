import Header from "./common/header";
import Intro from "./main page/intro";
import Delivery from "./main page/delivery";
import Services from "./main page/services";
import Success from "./main page/success_story";
import Revenue from "./main page/revenue";
import Highlights from "./main page/highligts";
import Partner from "./main page/partners";
import TopList from "./main page/top_list";
import Footer from "./common/footer";
import "../css/style.css";

const Main = () => {
  return (
    <>
      <body>
        <Header />
        <Intro />
        <Delivery />
        <Services />
        <Success />
        <Revenue />
        <Highlights />
        <Partner />
        <TopList />
        <Footer />
      </body>
    </>
  );
};

export default Main;

