const Header = () => {
  return (
    <>
      <header id="topHeader">
        <ul id="topInfo">
          <li>+974 98765432</li>
          <li>info@itecnology.com</li>
        </ul>

        <nav>
          <span className="logo">iTechnology</span>
          <div className="menu-btn-3" onclick="menuBtnFunction(this)">
            <span></span>
          </div>
          <div className="mainMenu">
            <a href="/">
              <span>Technology</span>
            </a>
            <a href="/">
              <span>Service</span>
            </a>
            <a href="/">
              <span>Portfolio</span>
            </a>
            <a href="/">
              <span>About Us</span>
            </a>
            <a href="/">
              <span>Career</span>
            </a>
            <a href="/">
              <span>Blog</span>
            </a>
            <a href="/">Work With Us</a>
          </div>
        </nav>
      </header>
    </>
  );
};
export default Header;
