import React from "react";
import "/Users/workspace/react_js/itechnology/itechnology/src/css/style.css";

const services = () => {
  let icons = [
    { class: "fas fa-laptop", title: "Stratagy and Consultant" },
    { class: "fas fa-users", title: "User Experience Design" },
    { class: "fas fa-mobile-alt", title: "Mobile App Development" },
    { class: "fab fa-chrome", title: "Web App Development" },
    { class: "fas fa-ribbon", title: "Quality Analysis and Testing" },
    { class: "fas fa-ticket-alt", title: "Application Management & Support" },
  ];
  return (
    <div>
      <section id="services">
        <h1 className="sec-heading">Our Services</h1>
        <ul>
          {icons.map((data) => {
            return (
              <li>
                <div>
                  <a href="#">
                    <i className={data.class}></i>
                    <span>{data.title}</span>
                  </a>
                </div>
              </li>
            );
          })}
        </ul>

        <div id="service-footer">
          <a href="#" className="brand-btn">
            View All Service
          </a>
        </div>
      </section>
    </div>
  );
};

export default services;

