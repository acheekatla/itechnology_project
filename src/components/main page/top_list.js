import React from "react";
import "/Users/workspace/react_js/itechnology/itechnology/src/css/style.css"
;

const topList = () => {
  const images = [
    { src: "https://www.dropbox.com/s/19czj59oq0orbfa/tm.png?raw=1" },
    { src: "https://www.dropbox.com/s/130734rofy1f261/tata.png?raw=1" },
    {
      src: "https://www.dropbox.com/s/k17kwv9hiu9w98d/Infosys_logo.png?raw=1",
    },
    { src: "https://www.dropbox.com/s/mm4cnforc4pvwac/Wipro_Logo.png?raw=1" },
    {
      src: "https://www.dropbox.com/s/n4scpig8b3tfqkq/Amazon_logo.svg?raw=1",
    },
  ];

  return (
    <div>
      <section id="topList" className="brand-logos">
        <h1 className="sec-heading">
          Recognition as Top Mobile Development Company
        </h1>
        <div>
          {images.map((data) => {
            return (
              <a>
                <img
                  src={data.src}
                  alt="Top 10 MobleApp Development Companies"
                  title="Top 10 MobleApp Development Companies"
                />
                <span>
                  Recognised Among Top 10 MobleApp Development Companies
                </span>
              </a>
            );
          })}
        </div>
      </section>
    </div>
  );
};

export default topList;
