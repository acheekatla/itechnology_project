import React from "react";
import "/Users/workspace/react_js/itechnology/itechnology/src/css/style.css";

const Revenue = () => {
  let logos = [
    {
      src: "https://www.dropbox.com/s/lmvtthec9yn0ti6/Allianz.png?raw=1",
      alt: "Allianz",
      title: "Work with Allianz",
    },
    {
      src: "https://www.dropbox.com/s/kotgq2u4qr34i2u/audi.jpg?raw=1",
      alt: "Audi",
      title: "Work with Audi",
    },
    {
      src: "https://www.dropbox.com/s/t5dapt3lkz7rdhe/BMW.png?raw=1",
      alt: "BMW",
      title: "Work with BMW",
    },
    {
      src: "https://www.dropbox.com/s/ocqbsbgj590ztyy/ESPN.png?raw=1",
      alt: "ESPN",
      title: "Work with ESPN",
    },
    {
      src: "https://www.dropbox.com/s/2maaqxijcmbaqxg/LG.png?raw=1",
      alt: "LG",
      title: "Work with LG",
    },
    {
      src: "https://www.dropbox.com/s/yn3gj203hrdjfu7/Logo_NIKE.png?raw=1",
      alt: "Nike",
      title: "Work with Nike",
    },
    {
      src: "https://www.dropbox.com/s/gfxa6exv7h1ro6q/Suzuki_logo.png?raw=1",
      alt: "Suzuki",
      title: "Work with Suzuki",
    },
    {
      src: "https://www.dropbox.com/s/b7vwmjf6e0owybv/Visa.svg?raw=1",
      alt: "Visa",
      title: "Work with Visa",
    },
  ];

  return (
    <div>
      <section id="revenue" className="brand-logos">
        <h1 className="sec-heading">
          We Drive Growth & Revenue for the Best Companies
        </h1>
        <div>
          {logos.map((data) => {
            return (
              <a>
                <img src={data.src} alt={data.alt} title={data.title} />
              </a>
            );
          })}
        </div>
      </section>
    </div>
  );
};

export default Revenue;

