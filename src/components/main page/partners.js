import React from "react";
import "/Users/workspace/react_js/itechnology/itechnology/src/css/style.css";

const partners = () => {
  let logos = [
    {
      src: "https://www.dropbox.com/s/mk5ca04seizpf8l/aws.svg?raw=1",
      alt: "Work with AWS",
      title: "Our Work",
    },
    {
      src: "https://www.dropbox.com/s/r9utt5nj9k9m1t8/Dell.png?raw=1",
      alt: "Dell",
      title: "Work with Dell",
    },
    {
      src: "https://www.dropbox.com/s/umw9g0zgm1ecfvn/Intel.png?raw=1",
      alt: "intel",
      title: "Work with intell",
    },
    {
      src: "https://www.dropbox.com/s/x0hrha2dosey99z/ibm.png?raw=1",
      alt: "IBM",
      title: "Work with IBM",
    },
    {
      src: "https://www.dropbox.com/s/ekzu1wcki6jziay/Microsoft.svg?raw=1",
      alt: "Microsoft",
      title: "WWork with Microsoft",
    },
    {
      src: "https://www.dropbox.com/s/lvl5cp14i3v0wgi/Nasscom.png?raw=1",
      alt: "Nasscom",
      title: "Work with Nasscom",
    },
    {
      src: "https://www.dropbox.com/s/h66k9jaaknxaum4/Samsung.png?raw=1",
      alt: "Samsung",
      title: "Work with Samsung",
    },
    {
      src: "https://www.dropbox.com/s/86cbtf78khj0q9z/Nvidia.png?raw=1",
      alt: "Nvidia",
      title: "Work with Nvidia",
    },
  ];

  return (
    <div>
      <section id="partners" className="brand-logos">
        <h1 className="sec-heading">Our Partners</h1>
        <div>
          {logos.map((data) => {
            return (
              <a>
                <img src={data.src} alt={data.alt} title={data.title} />
              </a>
            );
          })}
        </div>
      </section>
    </div>
  );
};

export default partners;

